# django
from django.apps import AppConfig


class ApiConfig(AppConfig):
    """Config for api."""

    name = 'server.apps.api'
