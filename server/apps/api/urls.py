# django
from django.conf.urls import url
from django.urls import include

app_name = 'api'
urlpatterns = [
    url('v1/', include(('server.apps.api.v1.urls', app_name), namespace='api_v1')),
]
