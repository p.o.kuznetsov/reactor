from django.contrib.auth.backends import UserModel
from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from server.apps.reactor.structs import Series, Substance
from server.apps.reactor import models


class SubstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Substance
        fields = (
            'code',
            'latex_code',
            'description',
        )


class FilledSubstanceSerializer(DataclassSerializer):
    class Meta:
        dataclass = Substance


class SeriesSerializer(DataclassSerializer):
    class Meta:
        dataclass = Series


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = (
            'username',
            'password',
        )
