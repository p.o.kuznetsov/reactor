from django.contrib.auth.backends import UserModel
from django.http import HttpResponse
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView

from server.apps.reactor.core import export_to_xls
from server.apps.api.v1 import serializers
from server.apps.reactor import core
from server.apps.reactor import models


class SubstanceViewSet(viewsets.ModelViewSet):
    queryset = models.Substance.objects.order_by('-code')
    serializer_class = serializers.SubstanceSerializer


class CalculateView(APIView):
    def post(self, request):
        serializer = serializers.FilledSubstanceSerializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        substances = serializer.validated_data
        series = core.compute(substances)
        return Response(data=serializers.SeriesSerializer([series], many=True).data)


class RegisterView(APIView):
    permission_classes = ()

    def post(self, request):
        serializer = serializers.RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserModel.objects.create_user(**serializer.validated_data)
        return Response({'info': 'ok'}, status=status.HTTP_201_CREATED)


class ExportView(APIView):
    def post(self, request):
        serializer = serializers.FilledSubstanceSerializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        substances = serializer.validated_data
        series = core.compute(substances)
        wrapper = export_to_xls(series, substances)
        filename = f'calculation.xlsx'
        response = HttpResponse(
            wrapper.read(),
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        )
        response['Content-Disposition'] = f'attachment; filename={filename}'
        return response
