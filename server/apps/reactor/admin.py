from django.apps import apps
from django.contrib import admin

for model in apps.get_models():
    if model._meta.app_label == 'reactor':
        admin.site.register(model, admin.ModelAdmin)
