from django.apps import AppConfig


class ReactorConfig(AppConfig):
    name = 'server.apps.reactor'
