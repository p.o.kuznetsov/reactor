import logging
from io import BytesIO
from typing import List, Dict

import numpy as np
from xlsxwriter import Workbook

from server.apps.reactor.structs import Substance, Series

logger = logging.getLogger('django')


def compute(substances: List[Substance]) -> Series:
    substance_by_code: Dict[str, float] = {substance.code: substance.value for substance in substances}
    q_range = np.arange(
        substance_by_code['Q_min'],
        substance_by_code['Q_max'] + substance_by_code['delta_Q'],
        substance_by_code['delta_Q'],
    )
    c_range = np.arange(
        substance_by_code['C_min'],
        substance_by_code['C_max'] + substance_by_code['delta_C'],
        substance_by_code['delta_C'],
    )
    q_values, c_values = np.meshgrid(
        q_range,
        c_range,
    )
    t_values = [
        [
            formula(
                v=substance_by_code['V_r'],
                k1=substance_by_code['k_1'],
                q=q,
                c=c,
                k2=substance_by_code['k_2'],
            )
            for q, c in zip(q_list, c_list)
        ]
        for q_list, c_list in zip(q_values, c_values)
    ]
    return Series(
        x=list(q_range),
        y=list(c_range),
        z=t_values,
    )


def formula(v, k1, q, c, k2):
    return 2 * v * k1 * q * c / ((v * k1 + q) * (v * k2 + q))


def export_to_xls(series: Series, substances: List[Substance]):
    output = BytesIO()
    wb = Workbook(output, {'in_memory': True})
    sheet = wb.add_worksheet()

    for i, substance in enumerate(substances):
        sheet.write(i, 0, substance.code)
        sheet.write(i, 1, substance.value)

    result_row = i + 4
    headers = ['Q/С'] + [str(x) for x in series.x]

    for i, header in enumerate(headers):
        sheet.write(result_row, i, header)
    result_row += 1

    for i, y in enumerate(series.y):
        sheet.write(i + result_row, 0, round(y, 2))
        for j, x in enumerate(series.x):
            sheet.write(i + result_row, j + 1, round(series.z[i][j], 2))
    wb.close()
    output.seek(0)
    return output
