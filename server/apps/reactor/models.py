from django.db import models


class Substance(models.Model):
    code = models.CharField(max_length=8)
    latex_code = models.CharField(max_length=32)
    description = models.TextField()

    def __str__(self) -> str:
        return f'id={self.id}, code={self.code}'
