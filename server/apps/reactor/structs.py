from dataclasses import dataclass
from typing import List


@dataclass
class Substance:
    code: str
    value: float


@dataclass
class Series:
    x: List[float]
    y: List[float]
    z: List[List[float]]
