LANGUAGE_CODE = 'en-us'

USE_I18N = True
USE_L10N = True

LANGUAGES = (('en', 'English'),)

LOCALE_PATHS = ('locale/',)

USE_TZ = False
TIME_ZONE = 'Europe/Moscow'
