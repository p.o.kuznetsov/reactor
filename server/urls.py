# django
from django.contrib import admin
from django.conf import settings
from django.urls import path
from django.conf.urls import include, url
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='api/v1/documentation', permanent=False)),
    url('api/', include('server.apps.api.urls', namespace='api')),
]

if settings.DEBUG:  # pragma: no cover
    import debug_toolbar  # noqa: WPS433

    urlpatterns.extend([
        path('__debug__/', include(debug_toolbar.urls)),
    ])
