import { ILogin, ILoginResponse, IRegisterResponse } from './interfaces';
import { ISeries, ISubstance, IFilledSubstance } from '@/interfaces';
import axios, { AxiosRequestConfig } from 'axios';
import store from './store';
import router from "./router"

const baseURL = process.env.VUE_APP_BASE_URL;

const client = axios.create({
  baseURL,
});

const auth = axios.create({
  baseURL,
});

client.interceptors.request.use((config: AxiosRequestConfig) => {
  // @ts-ignore
  const userStore = store.state.UserModule
  if (!userStore.status.loggedIn) {
    router.push({name: 'Login'});
  }

  // @ts-ignore
  config.headers.Authorization = `Token ${userStore.user.token}`
  return config
})

export async function getSubstances(): Promise<ISubstance[]> {
  const response = await client.get('/substances');
  return response.data as ISubstance[];
}

export async function computeSeries(substances: IFilledSubstance[]): Promise<ISeries[]> {
  const response = await client.post('/compute', substances);
  const appStore = store.state.ApplicationModule;
  return response.data as ISeries[];
}

export async function makeReport(substances: IFilledSubstance[]){
  const response = await client.post('/export/xlsx', substances, {responseType: 'arraybuffer',}).then(res => {
    const blob = new Blob([res.data])
        const link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.setAttribute('download', 'template.xlsx');
        document.body.appendChild(link);
        link.click();
  })
}

export async function loginUser(loginData: ILogin): Promise<ILoginResponse> {
  const response = await auth.post('/auth/login', loginData);
  return response.data as ILoginResponse;
}

export async function registerUser(loginData: ILogin): Promise<IRegisterResponse> {
  const response = await auth.post('/auth/register', loginData);
  return response.data as IRegisterResponse;
}
