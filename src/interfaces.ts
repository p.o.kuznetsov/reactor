export interface ISubstance {
    code: string;
    latexCode: string;
    description: string;
}

export interface IFilledSubstance extends ISubstance {
    value: string;
}

export interface ISeries {
    type?: string;
    hovertemplate?: string;
    x: Array<Array<number>>;
    y: Array<Array<number>>;
    z: Array<Array<number>>;
}

export interface IUser {
    username: string;
    token: string;
}

export interface ILogin {
    username: string;
    password: string;
}

export interface ILoginResponse {
    token: string;
}

export interface IRegisterResponse {
    info: string;
}