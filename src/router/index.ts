import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Layout from '@/components/Layout/Layout.vue';
import Login from "@/pages/Login/Login.vue";
import Dashboard from "@/pages/Dashboard/Dashboard.vue";


Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: 'login',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard,
      },
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
