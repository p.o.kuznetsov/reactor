import Vue from 'vue';
import Vuex from 'vuex';
import ApplicationModule from './modules/application';
import SubstanceModule from './modules/substances';
import UserModule from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    ApplicationModule,
    SubstanceModule,
    UserModule,
  },
});
