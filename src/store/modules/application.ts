import { Module, VuexModule, Mutation } from 'vuex-module-decorators';



@Module({ name: 'ApplicationModule' })
export default class GraduationModule extends VuexModule {
  public drawer = false;
  public requestTimeStart = 0

  @Mutation
  public toggleDrawer() {
    this.drawer = !this.drawer;
  }

  @Mutation
  public start() {
    this.requestTimeStart = Date.now();
  }
}
