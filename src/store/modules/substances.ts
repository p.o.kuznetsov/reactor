import { IFilledSubstance, ISeries, ISubstance } from './../../interfaces';
import { Module, VuexModule, MutationAction, Action, Mutation } from 'vuex-module-decorators';
import { computeSeries, getSubstances, makeReport } from '@/api';



@Module({ name: 'SubstanceModule' })
export default class SubstanceModule extends VuexModule {
  public requestTimeEnd = 0;
  public substances: IFilledSubstance[] = [];
  public series: ISeries[] = [];

  @Action
  public report(substances: IFilledSubstance[]) {
    makeReport(substances);
  }

  @MutationAction({ mutate: ['series'] })
  public async calculateSeries(substances: IFilledSubstance[]) {
    const series: ISeries[] = await computeSeries(substances);

    return { series };
  }

  @MutationAction({ mutate: ['substances'] })
  public async fetchSubstances() {
    const substances: ISubstance[] = await getSubstances();
    const filledSubstances: IFilledSubstance[] = [];
    substances.forEach((substance: ISubstance) => {
      filledSubstances.push({
        code: substance.code,
        latexCode: substance.latexCode,
        description: substance.description,
        value: '',
      })
    });

    // @ts-ignore
    this.state.requestTimeStart = Date.now();
    return { substances: filledSubstances };
  }
}
