import { IUser, ILogin } from './../../interfaces';
import { Module, VuexModule, MutationAction, Mutation, Action } from 'vuex-module-decorators';
import { loginUser, registerUser } from '@/api';
import router from './../../router';

const storedUser = localStorage.getItem('user');

@Module({ name: 'UserModule' })
export default class UserModule extends VuexModule {
  public status = storedUser ? { loggedIn: true } : { loggedIn: false };
  public user = storedUser ? JSON.parse(storedUser) : null;

  @MutationAction({ mutate: ['user'] })
  public async login(loginData: ILogin) {
    const token = await loginUser(loginData);
    const user: IUser = {
        username: loginData.username,
        token: token.token,
    }
    localStorage.setItem('user', JSON.stringify(user));

    // @ts-ignore
    this.state.status.loggedIn = true;
    try {
        return { user };
    }
    finally {
        router.push('/dashboard')
    }
  }

  @Action
  public async register(registerData: ILogin) {
      await registerUser(registerData);
  }


  @Mutation
  public logout() {
    localStorage.removeItem('user');
    this.status.loggedIn = false;
    this.user = null;
  }

}

